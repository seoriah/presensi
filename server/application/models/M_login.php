<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_login extends CI_Model{

  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  /**
   * [check_users_petugas description]
   * @param  [type] $username [description]
   * @param  [type] $password [description]
   * @return [type]           [description]
   */
  public function admin_verify($username, $password)
  {
    return $this->db->get_where('user', array('username' => $username, 'password' => sha1($password)));
  }

  /**
   * [check_users_dosen description]
   * @param  [type] $username [description]
   * @param  [type] $password [description]
   * @return [type]           [description]
   */
  public function dosen_verify($username, $password)
  {
    return $this->db->get_where('dosen', array('username' => $username, 'password' => sha1($password)))->row();
  }

  public function get_setting()
  {
    return $this->db->get('setting')->row();
  }
}
