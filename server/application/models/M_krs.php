<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_krs extends CI_Model{

  protected $table = 'krs';
  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  

  public function get_angkatan()
  {
  	$this->db->group_by('angkatan');
    return $this->db->get('mahasiswa')->result();
  }


  public function ajax_save_krs($nim, $tahun, $makul)
  {
   $data = array('nim' => $nim, 'kode_makul' => $makul, 'id_tahun_akademik' => $tahun); 
    return $this->db->insert('krs', $data);
  }
}