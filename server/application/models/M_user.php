<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_user extends CI_Model{

  protected $table = 'user';
  public function __construct()
  {
    parent::__construct();
    $this->load->database();
  }

  public function delete($id)
  {
    $this->db->where('username', $id);
    $this->db->delete($this->table);
  }

  public function add_user()
  {
    $nama      = $this->input->post('nama');
    $username   = $this->input->post('username');
    $pass = $this->input->post('password');
    $data = array('nama_user' => ucwords($nama),
    'username' => strtolower($username),
    'password' => sha1($pass));
    return $this->db->insert($this->table, $data);
  }

  public function get_user()
  {
    return $this->db->get($this->table)->result();
  }

  

}