
<div class="">
  
  <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-6 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="row x_title">
            <h3>Jadwal Mata Kuliah</h3>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
          <div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>HARI</th>
                  <th>MATA KULIAH</th>
                  <th width="50" align="center">KELAS</th>
                  <th align="center" width="50">#</th>

                </tr>
              </thead>
              <tbody id="data-jadwal">
               
                
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-6 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="row x_title">
            <h3>Data Presensi</h3>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
          <div class="table-responsive">
            <table class="table table-bordered">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Nim</th>
                  <th>Nama Lengkap</th>
                  <th>Status</th>

                </tr>
              </thead>
              <tbody id="data-presensi">
               
                
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  function load_jadwal(){
      $.ajax({
        url: "<?php echo base_url()?>ajax/ajax_dosen/ajax_load_jadwal",
        success: function (html) {
          console.log(html);
          $('#data-jadwal').html(html);
        }
      });
  }

  function load_presensi(id,makul,kelas) {
      $.ajax({
        url: "<?php echo base_url()?>ajax/ajax_dosen/ajax_load_presensi",
        type: "post",
        data:{'id':id,'makul':makul, 'kelas':kelas},
        success: function (html) {
          console.log(html);
          $('#data-presensi').html(html);
        }
      });
  }

  $(document).ready(function() {
    load_jadwal();
  });
</script>
<!-- <script type="text/javascript">
  function load_data_presensi() {
    var id = $('[name=kelas]').val();
    $.ajax({
      url: "<?php echo base_url()?>ajax/ajax_dosen/get_presensi",
      type: "post",
      dataType:"html",
      data: {"id":id},
      success: function(html) {
        console.log(html);
        $("#show-data").html(html);
      },
      error: function (error) {
        console.error(error);
      }

    });
  }
</script> -->