<div class="">
  
  <div class="clearfix"></div>
    <div class="row">

      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="row x_title">

            <div class="col-md-6">
              <div class="input-group">
                <select name="kelas" id="" class="form-control">
                  <?php foreach ($jadwal as $key ): ?>
                    <option value="<?php echo $key->id_jadwal ?>"><?php echo strtoupper($key->nama_makul) ?> - <?php echo strtoupper($key->kelas) ?></option>
                  <?php endforeach ?>
                </select>
                <span class="input-group-btn">
                  <button  class="btn btn-primary" id="btn-scanner">ON</button>
                </span>
              </div>
            </div>
      
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="row">
              <div class="col-md-6">
                <canvas style="width: 100%; height: 100%;" id="webcodecam-canvas"></canvas>
                <div class="scanner-laser laser-rightBottom" style="opacity: 0.5;"></div>
                <div class="scanner-laser laser-rightTop" style="opacity: 0.5;"></div>
                <div class="scanner-laser laser-leftBottom" style="opacity: 0.5;"></div>
                <div class="scanner-laser laser-leftTop" style="opacity: 0.5;"></div>
              </div>
              <div class="col-md-6">
                <div class="bs-example" data-example-id="simple-jumbotron" style="width: 100%;  height: 100%">
                    <div class="jumbotron text-center" style="width: 100%; height: 100%">
                      <h1 id='h1'>PENTING</h1>
                      <p id='p'>Pilih daftar kelas dan tekan <b>ON</b> untuk scan kartu</p>
                    </div>
                  </div>
              </div>
            </div>
            
           <!--  <div class="col-md-6">
                        <select style="width: 100%" class="form-control" id="camera-select"></select>
               </div>
          </div> -->
      </div>
    </div>

  </div>



<script>
$(document).ready(function() {
    var arg = {
      // grayScale: 0,
      frameRate: 25,
      // contrast: 50,
      zoom: -1,
      beep: '<?php echo base_url()?>assets/audio/beep.mp3',
      decoderWorker: '<?php echo base_url()?>assets/js/DecoderWorker.js',
      // flipHorizontal: true,
      resultFunction: function(res) {
          doPresensi(res.code);
      }
    };

    //Initialize new Class
    var decoder = $("canvas").WebCodeCamJQuery(arg).data().plugin_WebCodeCamJQuery;
    decoder.buildSelectMenu(document.createElement('select'), 'environment|back').init(arg);
    //if Button click
    $("#btn-scanner").click(function () {
      if ($(this).html() == 'ON') {
        if (!decoder.isInitialized()) {
            console.log('Scanning...');
        } else {
            console.log('Scanning...');
            decoder.play();   
            $(this).html('OFF');
        }   
      }else{
          decoder.stop();
          $(this).html('ON');
      }
    });
    //if Select change
    $("[name=kelas]").change(function () {
      if (decoder.isInitialized()) {
          decoder.stop();
          $("#btn-scanner").html('ON');
      }
    });
    //do save presensi data
    function doPresensi(data) {
      var jadwal = $("[name=kelas]").val();
      $.ajax({
        url: "<?php echo base_url()?>ajax/ajax_presensi/do_presensi",
        data: {'data':data, 'jadwal':jadwal},
        type: "post",
        dataType: "json",
        success: function (respone) {
          if(respone.result == 'warning'){
            alert('QR Code Not Invalid');
          }else if(respone.result == 'tersimpan'){
            alert('Presensi '+respone.nama+' Sudah Tersimpan');
          }else{
            $('#h1').html(respone.nim);
            $('#p').html(respone.nama+' Success');
            // alert('Presensi '+respone.nama+' Success');
          }
          // $(".jumbotron").html(respone);
        },
        error: function (error) {
          console.log(error)
        }
      });
    }
  });
</script>