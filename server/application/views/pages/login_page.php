<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Aplikasi Presensi</title>
    
    <!-- Bootstrap -->
    <link href="<?php echo base_url() ?>assets/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo base_url() ?>assets/css/font-awesome.min.css" rel="stylesheet">
    <!-- Custom Theme Style -->
    <link href="<?php echo base_url() ?>assets/css/custom.min.css" rel="stylesheet">
  </head>

  <body class="login">
    <div>
      <div class="login_wrapper">
        <div class="form login_form">
          
          <section class="login_content">
            <form action="<?php echo base_url() ?>login" method="post">
              <h2>APLIKASI PRESENSI UJIAN</h2>
              <div class="separator">
                 
              </div>
              <div>
                <input type="text" class="form-control" placeholder="Username" required="" name="username" autofocus="" />
                <? echo form_error('username')?>
              </div>
              <div>
                <input type="password" class="form-control" placeholder="Password" required="" name="password" />
                <? echo form_error('password')?>
              </div>
              <div>
                <button type="submit" class="btn btn-default submit pull-right" name="submit">Log in</button>
              </div>

              <div class="clearfix"></div>

              <div class="separator">
                 
              </div>
            </form>
          </section>
        </div>
      </div>
    </div>
    

  </body>
</html>