
<div class="">
  
  <div class="clearfix"></div>
  
 
  <div class="x_panel">
    <div class="row x_title">
      <div class="col-md-12">
        <h3>Tambah User</h3>
      </div>
      <div class="clearfix"></div>
    </div>
    <div class="x_content">
      <div class="row">
      <div class="col-md-3">
        <form action="<?php echo base_url() ?>user/add" method="post">
          <div class="form-group col-md-12">
            <label class="control-label col-md-12">Nama Lengkap</label>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <input type="text" class="form-control" name="nama" autofocus="">
            </div>
          </div>
         <div class="form-group col-md-12">
            <label class="control-label col-md-12">Username</label>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <input type="text" class="form-control" name="username">
            </div>
          </div>
          <div class="form-group col-md-12">
            <label class="control-label col-md-12">Password</label>
            <div class="col-md-12 col-sm-12 col-xs-12">
              <input type="password" class="form-control" name="password">
            </div>
          </div>
          <div class="form-group col-md-12 ">
            <div class="pull-right">
              <button type="submit" class="btn btn-primary" name="submit">Tambah</button>
            </div>
          </div>
        </form>
          
      </div>

       <div class="col-md-9 col-sm-12 col-xs-12">
            
              <table class="table table-bordered">
                <thead>
                  <th>Nama Lengkap</th>
                  <th>Username</th>
                  <th align="center" width="50">Option</th>
                </thead>
                <tbody>
                    <?php foreach ($user as $key): ?>
                      <tr>
                        <td><?php echo $key->nama_user ?></td>
                        <td><?php echo $key->username ?></td>
                        <td align="center">
                          <div class="button-group">
                            <a href="<?php echo base_url()."user/delete/".$key->username.""?>"><i class="fa fa-trash"></i></a>
                          </div>
                        </td>
                      </tr>
                    <?php endforeach ?>
                </tbody>
              </table>
                
                
        </div>
    </div>
    </div>

    
  </div>
</div>

