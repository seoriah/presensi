<div class="">
  
  <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-4 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="row x_title">
            <div class="col-md-6">
              <h3></h3>
            </div>
           <!--  <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Settings 1</a>
                  </li>
                  <li><a href="#">Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul> -->
            <div class="clearfix"></div>
          </div>
          <div class="x_content">

          <table class="table">
            <thead>
              <tr>
                <th>#</th>
                <th>Tahun</th>
                <th>Periode</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>
              <?php $no=1 ?>
              <?php foreach ($tahun_akademik as $key): ?>
                  <tr>
                    <td><?php echo $no ?></td>
                    <td><?php echo $key->tahun ?></td>
                    <td><?php echo $key->periode == 1 ? 'Ganjil' : 'Genap'?></td>
                    <td><?php echo $key->status == 1 ? 'Aktif' : '-'?></td>
                  </tr>
                  <?php $no++ ?>
              <?php endforeach ?>
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>