<!-- <div class=""> -->
  
  <!-- <div class="clearfix"></div> -->
    <!-- <div class="row"> -->
      <div class="col-md-8 col-sm-12 col-xs-12">
        <!-- <div class="x_panel"> -->
          <!-- <div class="row x_title"> -->
            <div class="row">
            <div class="col-md-12">
              <h3>Data Keuangan</h3>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <div class="form-group">
              <label class="label-control col-md-12">Nim / Nama</label>
              <h3><?php echo $keuangan->nim?> - <?php echo $keuangan->nama_mahasiswa ?></h3>
            </div>
            <div class="form-group">
              <label for="nim" class="label-control col-md-12">Tahun Akademik</label>
              <h3><?php echo $keuangan->tahun?> - <?php echo $keuangan->periode==1?'Ganjil':'Genap' ?></h3>
            </div>
            <div class="form-group">
              <label for="nim" class="label-control col-md-12">Keterangan</label>
              <?php if ($keuangan->status==1) : ?>                
                <h3 class="text-success">LUNAS</h3>
              <?php else:?>
                <h3 class="text-danger">TIDAK LUNAS</h3>
                <div class="ln_solid"></div>
                <a class="btn btn-sm btn-success" href="<?php echo base_url()."keuangan/update/".$keuangan->nim ?>">Update Lunas</a>
              <?php endif?>
            </div>
        </div></div>  
      <!-- </div> -->
    <!-- </div> -->
 <!--  </div>
</div> -->