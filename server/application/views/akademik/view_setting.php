<div class="">
  
  <div class="clearfix"></div>
    <div class="row">
      <div class="col-md-5 col-sm-12 col-xs-12 col-md-offset-3">
        <div class="x_panel">
          <div class="row x_title">
            <div class="col-md-12">
              <h3>Setting Application</h3>
            </div>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
            <form action="<?php echo base_url() ?>akademik/update_setting" method="post">
              <div class="form-group">
                <label for="tahun-akademik">Tahun Akademik / Semester</label>
                <select name="tahun-akademik" id="" class="form-control col-md-12 col-sm-12 col-xs-12">
                  <?php foreach ($tahun_akademik as $key): ?>
                    <option value="<?php echo $key->id_tahun_akademik ?>"><?php echo $key->tahun ?> - <?php echo $key->periode==1?'Ganjil':'Genap' ?></option>
                  <?php endforeach ?>
                </select>
              </div>
              <div class="form-group">
                <label for="ujian" >Periode Ujian</label>
                <select name="jenis-ujian" id="" class="form-control col-md-12 col-sm-12 col-xs-12">
                  <option value="1" >UTS</option>
                  <option value="2" >UAS</option>
                </select>
              </div>
              <div class="form-group">
                <label for="ujian" >Status</label>
                <select name="status" id="" class="form-control col-md-12 col-sm-12 col-xs-12">
                  <option value="1" >Aktif</option>
                  <option value="0" >Tidak</option>
                </select>
              </div>
              <div class="ln_solid"></div>
              <div class="form-group">
                <div class=" pull-right">
                  <button type="submit" class="btn btn-primary" name="submit">Update</button>
                </div>
              </div>
            </form>
        </div>
      </div>
    </div>
  </div>
</div>