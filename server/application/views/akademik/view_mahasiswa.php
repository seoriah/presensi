
<div class="">
  
  <div class="clearfix"></div>
  <a href="<?php echo base_url() ?>mahasiswa/add" class="btn btn-sm btn-default" >Tambah</a>
    <div class="row">
      
      <div class="col-md-12 col-sm-12 col-xs-12">
        <div class="x_panel">
          <div class="row x_title">
             <!--  <div class="col-md-2 col-sm-12 col-xs-12">
                <select name="angkatan" id="" class="form-control" onchange="loadMahasiswa()">
                <?php 
                  $now = date('Y');
                  for ($i= $now; $i > 2010; $i--) { 
                    echo "<option value=".$i."?>".$i."</option>";
                  }
                 ?>
                </select>
              </div> -->
              <div class="col-md-4">
                <h3>Data Mahasiswa</h3>
              </div>
            <ul class="nav navbar-right panel_toolbox">
              <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
              </li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
                <ul class="dropdown-menu" role="menu">
                  <li><a href="#">Settings 1</a>
                  </li>
                  <li><a href="#">Settings 2</a>
                  </li>
                </ul>
              </li>
              <li><a class="close-link"><i class="fa fa-close"></i></a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="x_content">
          
          <table class="table table-responsive">
            <thead>
              <tr>
                <th align="center">#</th>
                <th>Nim</th>
                <th>Nama</th>
                <th>Prodi</th>
                <th>Angkatan</th>
                <th width="50" align="center">Option</th>
              </tr>
            </thead>
            <tbody id="show-data">
              <?php $no=1 ?>
              <?php foreach ($mahasiswa as $key): ?>
                <tr>
                  <td align="center"><?php echo $no ?></td>
                  <td><?php echo $key->nim ?></td>
                  <td><?php echo $key->nama_mahasiswa ?></td>
                  <td><?php echo ucwords($key->prodi) ?></td>
                  <td><?php echo $key->angkatan ?></td>
                  <td align="center">
                    <div class="button-group">
                      <a href="<?php echo base_url()."mahasiswa/delete/".$key->nim.""?>"><i class="fa fa-trash"></i></a>
                    </div>
                  </td>
                </tr>
                <?php $no++ ?>
              <?php endforeach ?>
              
            </tbody>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>