<!-- <div class="clearfix"></div> -->
<a href="<?php echo base_url() ?>jadwal" class="btn btn-sm btn-warning" ><i class='fa fa-mail-reply-all'></i>Kembali</a>
<div class="row">
  <div class="col-md-12 col-sm-12 col-xs-12">
    <div class="x_panel">
      <div class="x_title">
        <h2>Tambah Jadwal</small></h2>
        <ul class="nav navbar-right panel_toolbox">
          <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
          </li>
          <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false"><i class="fa fa-wrench"></i></a>
            <ul class="dropdown-menu" role="menu">
              <li><a href="#">Settings 1</a>
              </li>
              <li><a href="#">Settings 2</a>
              </li>
            </ul>
          </li>
          <li><a class="close-link"><i class="fa fa-close"></i></a>
          </li>
        </ul>
        <div class="clearfix"></div>
      </div>
      <div class="x_content">
        <br />
        <form id="demo-form2" data-parsley-validate class="form-horizontal form-label-left" method="post" action="<?php echo base_url('jadwal/add') ?>">
        <div class="form-group">
            <label for="nama" class="control-label col-md-3 col-sm-3 col-xs-12">Mata Kuliah<span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <select name="makul" id="" class="form-control">
                <?php foreach ($makul as $key): ?>
                  <option value="<?php echo $key->kode_makul?>"><?php echo $key->nama_makul ?></option>
                <?php endforeach ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label for="nama" class="control-label col-md-3 col-sm-3 col-xs-12">Dosen<span class="required">*</span></label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <select name="dosen" id="" class="form-control">
                <?php foreach ($dosen as $key): ?>
                  <option value="<?php echo $key->kode_dosen?>"><?php echo $key->nama_dosen ?></option>
                <?php endforeach ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hari">Hari<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <select name="hari" id="" class="form-control">
                <option value="senin">Senin</option>
                <option value="selasa">Selasa</option>
                <option value="rabu">Rabu</option>
                <option value="kamis">Kamis</option>
                <option value="jumat">Jum'at</option>
                <option value="sabtu">Sabtu</option>
                <option value="minggu">Minggu</option>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="control-label col-md-3 col-sm-3 col-xs-12" for="hari">Kelas<span class="required">*</span>
            </label>
            <div class="col-md-6 col-sm-6 col-xs-12">
              <select name="kelas" id="" class="form-control">
                <?php foreach (range('A', 'Z') as $char ): ?>
                  <option value="<?php echo $char ?>"><?php echo $char ?></option>
                <?php endforeach ?>
              </select>
            </div>
          </div>
        
          <div class="ln_solid"></div>
          <div class="form-group">
            <div class="col-md-6 col-sm-6 col-xs-12 col-md-offset-3">
              <button type="submit" class="btn btn-primary" name="submit">Tambah</button>
            </div>
          </div>

        </form>
        <?php if ( $this->session->flashdata('result') == true ):?>
          <div class="alert alert-success alert-dismissible fade in" role="alert">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">×</span>
            </button>
            <strong>Success !</strong> Jadwal was inserted!.
          </div>
       <?php endif ?>
      </div>
    </div>
  </div>
  
</div>




            