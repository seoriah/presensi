<?php

/*
 * Demo widget
 */
class Navbar_top extends Widget {

    public function display($data) {
        
        if (!isset($data['items'])) {
            $data['items'] = array('Home', 'About', 'Contact');
        }

        $this->view('widgets/navbar_top', $data);
    }
    
}