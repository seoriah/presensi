<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->library(array('form_validation','session'));
		$this->load->model('m_login', 'login');
	}

	public function index()
	{
        $this->form_validation->set_rules('username', 'Username', 'required|trim');
	    $this->form_validation->set_rules('password', 'Password', 'required');
	    $this->form_validation->set_error_delimiters('<span class="error text-error">', '</span>');
	    
	      if($this->form_validation->run()==FALSE)
	      {
	        $this->load->view('pages/login_page');
	      }else
	      {
	       $username = $this->input->post('username');
	       $password = $this->input->post('password');
	       
	       $cek = $this->login->admin_verify($username, $password);
	        
	        if($cek->num_rows() > 0)
	        {
	        	$setting = $this->login->get_setting();
	          	
	          	$this->session->set_userdata('admin_loggin', TRUE);
	          	$this->session->set_userdata('username',$username);
	          	$this->session->set_userdata('type',1);
		        $this->session->set_userdata('id_tahun_akademik', $setting->id_tahun_akademik);
		        $this->session->set_userdata('tahun_akademik', '');
	         	redirect('presensi');
	        }else
	        {
	        	$cek = $this->login->dosen_verify($username, $password);
	        	if (isset($cek)) {
	        		$setting = $this->login->get_setting();
	        		$this->session->set_userdata('username',$username);
		          	$this->session->set_userdata('dosen_loggin', TRUE);
		          	$this->session->set_userdata('kode_dosen',$cek->kode_dosen);
		          	$this->session->set_userdata('type',2);
		          	$this->session->set_userdata('id_tahun_akademik', $setting->id_tahun_akademik);
		          	$this->session->set_userdata('tahun_akademik', '');
		         	redirect('dosen/presensi');
	        	}else{

		         	echo " <script>
				            alert('Gagal Login: Cek username , password anda!');
				            history.go(-1);
				          </script>";  
	        		}
	        	}
	        }
	    
	}
}
