<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Krs extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_krs', 'krs');
		
	}


	public function index()
	{
		$this->load->model('m_akademik', 'akademik');
		$data['prodi'] = $this->akademik->get_prodi();
		$data['angkatan'] = $this->krs->get_angkatan();
		$this->template->content->view('akademik/view_krs' ,$data);
        $this->template->publish();
	}

	public function delete()
	{	
		$id = $this->uri->segment(3);
		$this->prodi->delete($id);
		redirect('prodi');
	}

	public function add()
	{

		$result = $this->prodi->add_prodi();
		if ($result) {
			redirect('prodi');
		}
	}


}
