<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Prodi extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_prodi', 'prodi');
	}


	public function index()
	{
		$data['prodi'] = $this->prodi->get_prodi();
		$this->template->content->view('akademik/view_prodi', $data);
        $this->template->publish();
	}

	public function delete()
	{	
		$id = $this->uri->segment(3);
		$this->prodi->delete($id);
		redirect('prodi');
	}

	public function add()
	{

		$result = $this->prodi->add_prodi();
		if ($result) {
			redirect('prodi');
		}
	}


}
