<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_mahasiswa extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_mahasiswa', 'mahasiswa');
	}
	
	public function load_mahasiswa()
	{
		$prodi = $this->input->post('prodi');
		$angkatan = $this->input->post('angkatan');
		$result = $this->mahasiswa->ajax_load_mahasiswa($prodi, $angkatan);
		if ($result) {
			foreach ($result as $key) {
				echo "<option onclick='load_data_krs($key->nim)' >".$key->nim." - ".$key->nama_mahasiswa."</option>";
			}
		}
	}


	// public function load_mahasiswa()
	// {
	// 	$prodi = $this->input->post('prodi');
	// 	$angkatan = $this->input->post('angkatan');
	// 	$result = $this->mahasiswa->ajax_load_mahasiswa($prodi, $angkatan);
	// 	if ($result) {
	// 		foreach ($result as $key) {
	// 			echo "<option onclick=".load_data_krs($key->nim).">".$key->nim." - ".$key->nama_mahasiswa."</option>";
	// 		}
	// 	}
	// }
}
