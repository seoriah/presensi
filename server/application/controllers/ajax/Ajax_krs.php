<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax_krs extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_krs', 'krs');
		$this->load->model('m_mahasiswa', 'mhs');
		$this->load->model('m_akademik', 'akademik');
	}
	
	function load_data_krs()
    {
        $nim            =  $this->input->post('nim');
        $mhs            =  $this->mhs->get_mahasiswa_by_nim($nim); 
        $thn            =  $this->akademik->ajax_get_setting();
        $temp			=  substr($thn->tahun, 5);
        $temp_smt		=  ($temp - $mhs->angkatan) * 2;
        $smt            =  $thn->periode==1?$temp_smt - 1 : $temp_smt;
        
        echo "
        <table >
        <tr>
         <td rowspan='2' width='70' align='center'><h1><b>".  strtoupper($mhs->kelas)."</b></h1></td>
            <th width=50>Nim</th><th>: ".  $mhs->nim."</th>
            <th width=100</th>
            <th width=100 >Prodi</td><th>: ".  ucwords($mhs->prodi)."</th>
        </tr>
        <tr>
            <th >Nama</th><th>: ".  ucwords($mhs->nama_mahasiswa)."</th>
            <th width=100></th>
            <th>Semester</th><th>: ".$smt."</th>
        </tr>
        </table>";

        $krs            =  $this->akademik->ajax_get_krs($nim, $thn->id_tahun_akademik);
        echo "
        <table class='table table-bordered table-responsive' id='daftarkrs'>
        <tr><th width='5'>No</th>
        <th width='80'>KODE MP</th>
        <th>NAMA MATAKULIAH</th>
        <th width=10>SKS</th>
        <th>DOSEN</th>
        <th width='10'>#</th></tr>";
        $sks=0;
        if($krs->num_rows()<1)
        {
            echo "<tr><td colspan=6>DATA KRS TIDAK DITEMUKAN</td></tr>";
        }
        else
        {
            $no=1;
            
            foreach ($krs->result() as $r)
            {
                echo "<tr id='krshide$r->id_krs'>
                    <td>$no</td>
                    <td>".  strtoupper($r->kode_makul)."</td>
                    <td>".  strtoupper($r->nama_makul)."</td>
                    <td align='center'>".  $r->sks."</td>
                    <td>".  strtoupper($r->nama_makul)."</td>
                    <td align='center'><a href='#'><i class='fa fa-trash-o' onclick='hapus($r->id_krs)'></i></a></td>
                    </tr>";
                $no++;
                $sks=$sks+$r->sks;
            }
        }
    echo"<tr><td colspan='3' align='right'><b>TOTAL SKS</b></td><td align='center'><b>$sks<b></td><td colspan=2></td></tr><tr>
      </table>";
        echo "<a onclick='loadtablemapel($nim)' class='btn btn-primary btn-sm'><i class='gi gi-shopping_cart'></i> Input KRS</a> ";
        // echo anchor('cetak/kum/'.$d['nim'].'/'.$semester_aktif,'<i class="gi gi-print"></i> Cetak KUM',array('class'=>'btn btn-success btn-sm'));
        // echo"</td>
        // </tr></table>
        // ";
    }

    function load_makul()
    {
        $nim = $this->input->post('nim');
        $mhs =  $this->mhs->get_mahasiswa_by_nim($nim); 
        echo"<button onclick='load_data_krs($nim)' class='btn btn-warning btn-sm'><i class='fa fa-mail-reply-all'></i> Kembali</button>
        <table class='table table-bordered'>
            <tr ><th colspan=5>DAFTAR MATAKULIAH</th></tr>
            <tr><th width=10>No</th><th width=20>Kode</th>
            <th>Nama Matakuliah</th>
            <th width=30 align=center>SKS</th><th>#</th></tr>";
            // dapatkan jumlah semester dari kosentrasi yang diminta
            // $data=  $this->db->get_where('akademik_konsentrasi',array('konsentrasi_id'=>$konsentrasi))->row_array();
            $th = $this->akademik->ajax_get_setting();
            $smt=8;
            for($i=1;$i<=$smt;$i++)
            {
                $makul = $this->akademik->ajax_get_makul($i);
                echo"<tr class='warning'><th colspan=9>SEMESTER $i</th></tr>";
                $no=1;
                foreach ($makul as $m)
                {
                    echo "<tr id='hide$m->kode_makul'><td>$no</td>
                        <td>".  strtoupper($m->kode_makul)."</td>
                        <td>".  ucwords($m->nama_makul)."</td>
                        <td align=center>".  $m->sks."</td>
                        <td width='10' align='center'><i style='cursor:pointer' class='fa fa-plus' onclick=ambil($nim,$th->id_tahun_akademik,'".$m->kode_makul."') title='Ambil Matakuliah'></i></td>

                         </tr>";
                    $no++;
                }
                            
            }
            echo "<table>";
    }

    public function ajax_save_krs()
    {
        $nim = $this->input->post('nim');
        $tahun = $this->input->post('tahun');
        $makul = $this->input->post('makul');
        $result = $this->krs->ajax_save_krs($nim, $tahun, $makul);
        if ($result) {
            echo "success";
        }
    }
}
