<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dosen extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->model('m_dosen', 'dosen');
	}

	public function index()
	{
		if ($this->session->userdata('type') == 1) {
			$data['dosen'] = $this->dosen->get_dosen();
			$this->template->content->view('dosen/view_dosen', $data);
	        $this->template->publish();
		}
	}
	
	public function delete()
	{	
		if ($this->session->userdata('type') == 1) {
			$id = $this->uri->segment(3);
			$this->dosen->delete($id);
			redirect('dosen');
		}
	}

	public function add()
	{
		if ($this->session->userdata('type') == 1) {
			if ($this->input->post()) {
				$this->dosen->add_dosen();
			}
			$this->template->content->view('dosen/add_dosen');
	        $this->template->publish();
		}
	}

	

	public function scanning()
	{
		if ($this->session->userdata('type') == 2) {
			
			$this->template->stylesheet->add(base_url('assets/css/scanner.css'));
			$this->template->javascript->add(base_url('assets/js/qrcodelib.js'));
			// $this->template->javascript->add(base_url('assets/js/webcodecamjs.js'));
			// $this->template->javascript->add(base_url('assets/js/scanner.js'));
			
			$this->template->javascript->add(base_url('assets/js/webcodecamjquery.js'));
			// $this->template->javascript->add(base_url('assets/js/scanner.js'));
			$kode = $this->session->userdata('kode_dosen');
			$tahun = $this->session->userdata('id_tahun_akademik');
			$data['jadwal'] = $this->dosen->get_kelas($tahun, $kode);
			$this->template->content->view('dosen/view_scanner', $data);
	        $this->template->publish();
		}
	}

	public function profile()
	{
		if ($this->session->userdata('type') == 2) {
			$data['dosen'] =$this->dosen->get_dosen_by_id($this->session->userdata('kode_dosen'));
			$this->template->content->view('dosen/view_profile', $data);
	        $this->template->publish();
		}
	}

	public function update_password()
	{
		if ($this->session->userdata('type') == 2) {
			$kode = $this->session->userdata('kode_dosen');
			$result = $this->dosen->update_password($kode);
			$this->session->set_flashdata('result', true);
			redirect('dosen/profile');
		}
	}

	public function presensi()
	{
		if ($this->session->userdata('type') == 2) {
			// $data[''] = $this->session->userdata('kode_dosen');
			// $data[''] = $this->session->userdata('id_tahun_akademik');

			//$data['jadwal'] = $this->dosen->get_kelas($tahun, $kode);
			// $data['presensi'] = $this->dosen->get_presensi($kode, $setting->id_tahun_akademik);
			$this->template->content->view('dosen/view_presensi');
	        $this->template->publish();
		}
	}

	public function view_qr()
	{
		$message = $this->uri->segment(3);
	      /*Check keys data to dabatabe*/
	      $keys = $this->dosen->get_setting();
	      if ($keys->status == 1) {
	        /*Load library and generate chipertext*/
	        $this->load->library('rsa');
	        $encoded = $this->rsa->rsa_encrypt ($message,  $keys->public,  $keys->modulo);
	        $data['qr'] = $this->__generate_qr($encoded);
	        $this->template->content->view('dosen/view_qr', $data);
	        $this->template->publish();
		}
	}
	protected function __generate_qr($data)
  	{
	    $this->load->library('ciqrcode');
	    $config['imagedir']    = 'application/third_party/qr/'; 
	    if (!file_exists($config['imagedir'])) {
	      mkdir($config['imagedir'], 777);
	    }
	    $this->ciqrcode->initialize($config);
	    $image_name=$data.'.png'; //buat name dari qr code sesuai dengan nim
	    $params['data']     = base64_encode($data); //data yang akan di jadikan QR CODE
	    $params['level']    = 'H'; //H=High
	    $params['size']     = 700;
	    $params['savename'] = FCPATH.$config['imagedir'].$image_name; 
	    $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE
	    return $this->ciqrcode->qrbase64($params['savename']);
	}
}
