<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_mahasiswa extends CI_Model{

  public function __construct()
  {
    parent::__construct();
  }

  /**
   * [get_keys description]
   * @return [type] [description]
   */
  public function get_keys()
  {
    return $this->db->get('setting')->row();
  }

  /**
   * [get_mahasiswa controller]
   * @param  boolean $param [description]
   * @return [type]         [description]
   */
  public function get_mahasiswa($param = FALSE)
  {
    if ($param === FALSE)
    {
      $query = $this->db->get('mahasiswa');
      return $query->result();
    }

    $query = $this->db->get_where('mahasiswa', array('nim' => $param));
    return $query->row();
  }

  

  /**
   * [get_tahun_akademik controller]
   * @param  boolean $param [description]
   * @return [type]         [description]
   */
  public function get_tahun_akademik($param = FALSE)
  {
    if ($param === FALSE)
    {
      $query = $this->db->get('tahun_akademik');
      return $query->result();
    }

    $query = $this->db->get_where('tahun_akademik', array('aktif' => 1));
    return $query->row();
  }

  /**
   * [get_tahun_akademik description]
   * @param  boolean $param [description]
   * @return [type]         [description]
   */
  public function get_tahun_akademik_by_id($param = FALSE)
  {
    $query = $this->db->get_where('tahun_akademik', array('id_tahun_akademik' => $param));
    return $query->row();
  }
  /**
   * [check_pembayaran description]
   * @param  string $nim [description]
   * @param  string $smt [description]
   * @return [type]      [description]
   */
  public function check_pembayaran($nim='', $tahun)
  {
    return $this->db->get_where('pembayaran', array('nim' => $nim, 'id_tahun_akademik' => $tahun))->row();
  }
  
  /**
   * [check_users description]
   * @param  [type] $nim      [description]
   * @param  [type] $password [description]
   * @return [type]           [description]
   */
  public function check_users($nim, $password)
  {
    return $this->db->get_where('account_mahasiswa', array('nim' => $nim, 'password' => sha1($password)));
  }

  public function do_presensi($param = array())
  {
    return $this->db->insert('presensi', $param);
  }

  public function cek_presensi($nim, $jadwal)
  {
    return $this->db->get_where('presensi', array('nim' => $nim , 'id_jadwal' => $jadwal) )->row();
  }
}
