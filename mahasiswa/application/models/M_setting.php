<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_setting extends CI_Model{

  public function __construct()
  {
    parent::__construct();
  }

  /**
   * [get_mahasiswa controller]
   * @param  boolean $param [description]
   * @return [type]         [description]
   */
  public function get_setting_mhs($param = FALSE)
  {
    $query = $this->db->get_where('setting_mahasiswa', array('nim' => $param));
    return $query->row();
  }

  /**
   * [check_data_ujian description]
   * @return [type] [description]
   */
  public function get_setting_app()
  {
    return $this->db->get('setting_app');
    return $query->row();
  }

  /**
   * [insert description]
   * @param  array  $params [description]
   * @return [type]         [description]
   */
  public function insert($params = array())
  {
    $data = array('nim' => $params[0], 
          'cetak_kartu' => 1,
          'modulo' => $params[1],
          'public' => $params[2],
          'private' => $params[3],
          'qr' => $params[4]);
    $this->db->insert('setting_mahasiswa', $data);
    return true;
  }
}
