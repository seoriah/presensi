<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_akademik extends CI_Model{

  public function __construct()
  {
    parent::__construct();
  }
  
  /**
   * [get_kelas controller]
   * @param  [string] $nim        [description]
   * @param  [integer] $smt_aktif [description]
   * @return [type]               [description]
   */
  public function get_kelas_by_id($id)
  {
    $query  = $this->db->select('*')
              ->from('kelas')
              ->join('makul', 'makul.kode_makul=kelas.kode_makul', 'LEFT')
              ->join('data_dosen', 'data_dosen.id_dosen=kelas.id_dosen', 'LEFT')
              ->where('id_kelas', $id)
              ->get();
    return $query->row_array();
  }

  /**
   * [get_kelas controller]
   * @param  [string] $nim        [description]
   * @param  [integer] $smt_aktif [description]
   * @return [type]               [description]
   */
  public function get_kelas($nim, $tahun)
  {
    $params = array('krs.nim' => $nim, 'krs.id_tahun_akademik' => $tahun);
    
    $query  = $this->db->select('*')
              ->from('krs')
              ->join('mahasiswa', 'mahasiswa.nim=krs.nim')
              ->join('makul', 'makul.kode_makul=krs.kode_makul')
              ->where($params)
              ->get();
    return $query->result();
  }


  public function get_jadwal($kelas, $makul, $tahun)
  {
    $this->db->join('dosen', 'dosen.kode_dosen=jadwal.kode_dosen');
    $this->db->where(array('jadwal.kelas' => $kelas, 'jadwal.kode_makul' => $makul, 'jadwal.id_tahun_akademik' => $tahun));
    return $this->db->get('jadwal')->row();
  }
  /**
   * [get_tahun_akademik controller]
   * @param  boolean $param [description]
   * @return [type]         [description]
   */
  public function get_tahun_akademik($param = FALSE)
  {
    if ($param === FALSE)
    {
      $query = $this->db->get('tahun_akademik');
      return $query->result();
    }

    $query = $this->db->get_where('tahun_akademik', array('aktif' => 1));
    return $query->row();
  }
  /**
   * [get_mahasiswa controller]
   * @param  boolean $param [description]
   * @return [type]         [description]
   */
  public function get_data_mahasiswa($param = FALSE)
  {
    return $this->db->get_where('mahasiswa', array('nim' => $param))->row();
  }
  

  /**
   * get setting application from database
   * @return data 
   */
  public function get_setting_app()
  {
    return $this->db->get('setting')->row();
  }
}
