<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class M_login extends CI_Model{

  public function __construct()
  {
    parent::__construct();
  }

  /**
   * [get_mahasiswa controller]
   * @param  boolean $param [description]
   * @return [type]         [description]
   */
  public function get_data_mahasiswa($param = FALSE)
  {
    return $this->db->get_where('mahasiswa', array('nim' => $param))->row();
  }
  /**
   * Verify Model
   * @param  [type] $nim      [text]
   * @param  [type] $password [text]
   * @return [type]           [data]
   */
  public function verify($nim, $password)
  {
    return $this->db->get_where('mahasiswa', array('nim' => $nim, 'password' => sha1($password)))->row();
  }

  /**
   * get setting application from database
   * @return data 
   */
  public function get_setting_app()
  {
    return $this->db->get('setting')->row();
  }
}
