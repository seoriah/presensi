<!DOCTYPE HTML>
<html>
    <head>
        <!-- meta keywords specific to the page -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
      	<meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
        <meta name="viewport" content="width=device-width" />
        <!-- page title specific to the page -->
        <title>Aplikasi Presensi Ujian</title>
        <!--[if IE]> <script> (function() { var html5 = ("abbr,article,aside,audio,canvas,datalist,details," + "figure,footer,header,hgroup,mark,menu,meter,nav,output," + "progress,section,time,video").split(','); for (var i = 0; i < html5.length; i++) { document.createElement(html5[i]); } try { document.execCommand('BackgroundImageCache', false, true); } catch(e) {} })(); </script> <![endif]-->
        <!-- favicon -->
        <link rel="icon" type="image/png" href="<?php echo base_url('assets/img/logo.png'); ?>"/>
        <!-- template style -->
        <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/css/bootstrap.min.css'); ?>"/>
        <link type="text/css" rel="stylesheet" href="<?php echo base_url('assets/css/light-bootstrap-dashboard.css'); ?>">
        <!-- Fonts and icons     -->
        <link href="<?php echo base_url('assets/css/font-awesome.min.css'); ?>" rel="stylesheet">

    </head>
    <body>
        <div class="wrapper" style="margin-top:70px">
          <div class="content">
              <div class="container-fluid">
                  <div class="row">
                    <div class="col-md-6 col-md-offset-3">
                      <div class="card">
                        <div class="header">
                          <h4 class="title text-center text-primary">APLIKASI PRESENSI UJIAN</h4>
                          <hr>
                        </div>
                        <div class="content text-center">
                          
                          <h2>Kartu <?php echo $jenis_ujian ?></h2>
                          <h1>Belum Tersedia</h1>
                          <a class="btn btn-default" href="<?php echo base_url() ?>logout">LOGOUT</a>
                        </div>
                      </div>
                    </div>
                  </div>
              </div>
          </div>
        </div>




    </body>
</html>
