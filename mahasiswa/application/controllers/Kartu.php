<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kartu extends CI_Controller{

  public function __construct()
  {
    parent::__construct();
    $this->load->model('m_mahasiswa', 'mhs');
    $this->load->model('m_setting', 'setting');
    $this->load->model('m_akademik', 'akademik');
    if ($this->session->userdata('isLogin') == FALSE) {
      redirect('login');
    }
  }

  /**
   * [VieW qr code controller]
   * @return [type] [description]
   */
  public function view()
  {
    $nim = $this->session->userdata('nim');
    $tahun = $this->session->userdata('tahun_akademik');
    $result = $this->mhs->check_pembayaran($nim, $tahun);
    if ($result->status == 1) {
       $message = $nim."-".$tahun;
      /*Check keys data to dabatabe*/
      $keys = $this->mhs->get_keys();
      if ($keys->status == 1) {
        /*Load library and generate chipertext*/
        $this->load->library('rsa');
        $encoded = $this->rsa->rsa_encrypt ($message,  $keys->public,  $keys->modulo);
        $data['qr'] = $this->__generate_qr($nim, $encoded);
        $data['header_content'] = 'Kartu Ujian';
        $this->template->write_view('content', 'view_kartu_ujian', $data, TRUE);
        $this->template->render();
      }else{
        $data['jenis_ujian'] = $this->session->userdata('jenis_ujian') == 1 ? 'Ujian Tengah Semester' : 'Ujian Akhir Semester';
        $data['header_content'] = 'Kartu Ujian';
        $this->template->write_view('content', 'errors/html/error_tanggal_cetak', $data, TRUE);
        $this->template->render();
      }
    }else{
        $data['header_content'] = 'Kartu Ujian';
        $this->template->write_view('content', 'errors/html/error_kartu_ujian', $data, TRUE);
        $this->template->render();
      }
    
  }

  /**
   * [print description]
   * @return [type] [description]
   */
  public function print()
  {
   $nim = $this->session->userdata('nim');
    $tahun = $this->session->userdata('tahun_akademik');
    $result = $this->mhs->check_pembayaran($nim, $tahun);
    $mhs = $this->mhs->get_mahasiswa($nim);
    if ($result->status == 1) {
      $keys = $this->mhs->get_keys();
      if ($keys->status == 1) {
        {
        $this->load->library('pdf');
        $pdf = new FPDF('l','mm','custom');
        // membuat halaman baru
        $pdf->AddPage();
        $pdf->setTitle('Kartu Ujian Mahasiswa');
        // setting jenis font yang akan digunakan
        $pdf->Image('assets/img/logo.png',2,2,-1000);
        $pdf->Image(APPPATH.'third_party/kartu/'.$nim.'.png',90,2,-600);
        $pdf->SetFont('Arial','B',11);
        // mencetak string 
        $jenis = $this->session->userdata('jenis_ujian') == 1 ? 'KARTU UJIAN TENGAH SEMESTER' : 'KARTU UJIAN AKHIR SEMESTER';
        $pdf->Text(20, 7, $jenis);
        $pdf->SetFont('Arial','',10);
        $pdf->Text(20, 10, 'UNIVERSITAS NURUL JADID');
        $pdf->Text(20, 13, 'FAKULTAS TEKNIK');
        $pdf->SetFont('Arial','B',10);
        $tmp = $this->mhs->get_tahun_akademik_by_id($this->session->userdata('tahun_akademik'));
        $ket = $tmp->periode == 1 ? 'GANJIL' : 'GENAP';
        $pdf->Text(20, 16, 'TAHUN AKADEMIK '.$tmp->tahun.' - '.$ket);
        // $pdf->Cell(100,1,'KARTU UJIAN MAHASISWA',0,1,'L');
        // Memberikan space kebawah agar tidak terlalu rapat
        $pdf->Line(0,20,138,20);
        $pdf->SetFont('Arial','B',8);
        $pdf->Cell(0,12,'',0,1);
        $pdf->Cell(15,5,'Nim',0,0);
        $pdf->Cell(40,5,': '.$nim,0,1);
        $pdf->Cell(15,5,'Nama',0,0);
        $pdf->Cell(40,5,': '.$mhs->nama_mahasiswa,0,1);
        $pdf->Cell(15,5,'HARI',1,0,'C');
        $pdf->Cell(37,5,'MATA KULIAH',1,0,'C');
        $pdf->Cell(37,5,'DOSEN',1,1,'C');
        $pdf->SetFont('Arial','',7);
        $kelas = $this->akademik->get_kelas($nim, $this->session->userdata('tahun_akademik'));
         foreach ($kelas as $key) {
            $jadwal = $this->akademik->get_jadwal($key->kelas, $key->kode_makul, $tahun);
            $pdf->Cell(15,5,ucwords($jadwal->hari),1,0);
            $pdf->Cell(37,5,$key->nama_makul, 1,0);
            $pdf->Cell(37,5,$jadwal->nama_dosen,1,1,'C'); 
        }
        $pdf->Output('', $nim.'.pdf');
        }
    }
      
    }else{
      $data['header_content'] = 'Cetak Kartu';
      $this->template->write_view('content', 'errors/html/error_kartu_ujian', $data, TRUE);
      $this->template->render();
    }
    
  }

  /**
   * [generate_qr description]
   * @param  string $data [description]
   * @return [type]       [description]
   */
  protected function __generate_qr($nim, $data)
  {
    $this->load->library('ciqrcode');
    $config['imagedir']    = 'application/third_party/kartu/'; 
    if (!file_exists($config['imagedir'])) {
      mkdir($config['imagedir'], 777);
    }
    // $config['quality']   = true; //boolean, the default is true
    // $config['size']      = '1024'; //interger, the default is 1024
    // $config['black']   = array(224,255,255); // array, default is array(255,255,255)
    // $config['white']   = array(70,130,180); // array, default is array(0,0,0)
    $this->ciqrcode->initialize($config);
    $image_name=$nim.'.png'; //buat name dari qr code sesuai dengan nim
    $params['data']     = base64_encode($data); //data yang akan di jadikan QR CODE
    $params['level']    = 'H'; //H=High
    $params['size']     = 700;
    $params['savename'] = FCPATH.$config['imagedir'].$image_name; 
    $this->ciqrcode->generate($params); // fungsi untuk generate QR CODE
    return $this->ciqrcode->qrbase64($params['savename']);
  }
  
}
