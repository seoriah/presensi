<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Mahasiswa extends CI_Controller{
  public function __construct()
  {
    parent::__construct();
    $this->load->model('mmahasiswa', 'mahasiswa');
  }

  public function get_mahasiswa()
  {
    $result = $this->mahasiswa->get_mahasiswa();
    echo json_encode($result);
  }
}
