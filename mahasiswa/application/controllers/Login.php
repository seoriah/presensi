<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

	function __construct()
	{
		parent::__construct();
		$this->load->helper('form');
		$this->load->model('m_login', 'login');
		$this->load->model('m_akademik', 'akademik');
	}

	/**
	 * [index page for this controller]
	 * @return [type] [description]
	 */
	public function index()
	{
		/*Get application setting*/
		
		
				$this->load->library('form_validation');
				$this->form_validation->set_rules('nim', 'NIM', 'required|trim', array('required' => '* %s tidak boleh kosong'));
				$this->form_validation->set_rules('password', 'Password', 'required|trim', array('required' => '* %s tidak boleh kosong'));
				$this->form_validation->set_error_delimiters('<span class="text-warning">', '</span>');
				if($this->form_validation->run() == FALSE){
					$this->load->view('login_page_view');
				}else{
					$nim      = $this->input->post('nim');
					$password = $this->input->post('password');
					/** Load login model and check users on database*/
					$users = $this->login->verify($nim, $password);
					if (isset($users)) {
						$mhs = $this->akademik->get_data_mahasiswa($nim);
						// if ($mhs->status  == 1) {
						// 	$thn = $this->akademik->get_tahun_akademik(TRUE);
						// 	if (isset($thn)) {
					 //    		$temp = (substr($thn->tahun, 5) - $mhs->angkatan) * 2;
					 //    		$smt_aktif = $thn->keterangan == 1 ? $temp-1 : $temp;
					 //    	}
					 $setting = $this->akademik->get_setting_app();
					 if (isset($setting)) {
						// if ($setting->status == 0) {
						// 	$data['jenis_ujian'] = $setting->jenis_ujian == 1 ? 'Ujian Tengah Semester' : 'Ujian Akhir Semester';
						// 	$this->load->view('errors/html/error_tanggal_cetak',$data);
						// }else{
					    	$session = array(
								'isLogin' => TRUE,
								'nim' => $nim,
								'nama' => $mhs->nama_mahasiswa,
								'jenis_ujian' => $setting->jenis_ujian,
								'tahun_akademik' =>$setting->id_tahun_akademik);
						  	$this->session->set_userdata($session);
							redirect('kelas');
					 //  	}else{
						// 	echo "<script>
						// 		alert('You cannot loggin!');
						//       	history.go(-1);
						// 	</script>";
						// }
					
					}else {
						echo "<script>
								alert('Pastikan Nim / Password Anda benar!');
						      	history.go(-1);
							</script>";
					}
					
				
			}
		}
	}

}
